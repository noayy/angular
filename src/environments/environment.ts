// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBCcwOvnaJW9YT7HU0OP_JWpQmK0pGqcuY",
    authDomain: "hello2-6d33e.firebaseapp.com",
    databaseURL: "https://hello2-6d33e.firebaseio.com",
    projectId: "hello2-6d33e",
    storageBucket: "hello2-6d33e.appspot.com",
    messagingSenderId: "986741688893",
    appId: "1:986741688893:web:5eaab9b0517c057b75f854"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
