import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private URL = "https://jsonplaceholder.typicode.com/posts/"; 

  constructor(private http:HttpClient) { }

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.URL);
  }

}
