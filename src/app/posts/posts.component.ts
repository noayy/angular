import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$:Observable<Post>

  constructor(private PostService:PostService) { }

  ngOnInit(): void {
    this.posts$ = this.PostService.getPosts();
  }

}
